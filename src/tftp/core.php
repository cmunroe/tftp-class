<?php

namespace tftp;

/**
 * The Core class for backing up switches.
 */
class core {

    protected $init = '/etc/init.d/tftpd-hpa ';

    /**
     * Control the TFTPD-HPA server
     *
     * @param string $action [start, stop, status, et cetera.]
     * @return string the log of starting the service.
     */
    protected function tftpd(string $action){

        return shell_exec($this->init . $action);

    }

    /**
     * Start the TFTPD-HPA server.
     *
     * @return this chain
     */
    public function start(){

        $this->tftpd('start');

        $this->tftpdStart = true;

        return $this;

    }

    /**
     * stop the TFTPD-HPA server.
     *
     * @return this chain.
     */
    public function stop(){

        $this->tftpd('stop');

        $this->tftpdStart = false;

        return $this;

    }

    /**
     * Set tftpd init location.
     *
     * @param [string] $path
     * @return this chain.
     */
    public function init(string $path){

        $this->init = $path . ' ';

        return $this;


    }

}